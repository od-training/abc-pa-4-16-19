import { HttpClient } from '@angular/common/http';
import { Injectable } from '@angular/core';
import { Observable } from 'rxjs';
import { map } from 'rxjs/operators';

import { Video } from './types';

const API_URL = 'http://api.angularbootcamp.com';

function convertAuthorToUppercase(video: Video) {
  return { ...video, author: video.author.toUpperCase() };
}

@Injectable({
  providedIn: 'root'
})
export class VideoDataService {

  constructor(private http: HttpClient) { }

  loadVideos(): Observable<Video[]> {
    return this.http
      .get<Video[]>(API_URL + '/videos')
      .pipe(
        map( (videos: Video[]) => videos.map(convertAuthorToUppercase) )
      )
    ;
  }
}
