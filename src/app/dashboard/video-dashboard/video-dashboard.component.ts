import { Component, OnInit } from '@angular/core';
import { Observable } from 'rxjs';

import { VideoDataService } from '../../video-data.service';
import { Video } from '../../types';
import { ActivatedRoute, Router } from '@angular/router';
import { map, tap } from 'rxjs/operators';

@Component({
  selector: 'app-video-dashboard',
  templateUrl: './video-dashboard.component.html',
  styleUrls: ['./video-dashboard.component.css']
})
export class VideoDashboardComponent implements OnInit {
  currentVideo: Observable<string>;

  videoList: Observable<Video[]>;

  constructor(svc: VideoDataService, route: ActivatedRoute, router: Router) {
    this.videoList = svc.loadVideos().pipe(
      tap(videos => {
        if (!route.snapshot.queryParamMap.get('selectedVideo')) {
          router.navigate([], {
            queryParams: { selectedVideo: videos[0].id }
          });
        }
      })
    );

    this.currentVideo = route.queryParamMap.pipe(
      map(params => params.get('selectedVideo'))
    );
  }

  ngOnInit() {
  }
}
