import { Component, Input } from '@angular/core';

import { Video } from '../../types';
import { Router } from '@angular/router';

@Component({
  selector: 'app-video-list',
  templateUrl: './video-list.component.html',
  styleUrls: ['./video-list.component.css']
})
export class VideoListComponent {
  @Input('videoListData') videos: Video[] | undefined;
  @Input() selectedVideo: string | undefined;

  constructor(private router: Router) { }

  videoClick(video: Video) {
    this.router.navigate([], {
      queryParams: { selectedVideo: video.id }
    });
  }

}
