import { Component, OnInit } from '@angular/core';
import { FormBuilder, FormGroup, Validators } from '@angular/forms';

@Component({
  selector: 'app-stat-filters',
  templateUrl: './stat-filters.component.html',
  styleUrls: ['./stat-filters.component.css']
})
export class StatFiltersComponent implements OnInit {

  statFiltersGroup: FormGroup;
  
  constructor(fb: FormBuilder) {
  
    this.statFiltersGroup = fb.group({
      partialVideoTitle: ['', Validators.minLength(3)]
    });
  }

  ngOnInit() {
  }

}
